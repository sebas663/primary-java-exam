package com.example;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import com.example.concurrency.ConcurrentMemoryStore;
import com.example.concurrency.ConcurrentMemoryStoreImpl;
import com.example.concurrency.Item;
import com.example.iterator.ComparableIterator;
import com.example.iterator.SequenceIterator;

@SpringBootApplication
public class PrimaryJavaExamApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrimaryJavaExamApplication.class, args);
	}

	private final ConcurrentMemoryStore store = new ConcurrentMemoryStoreImpl();
	private int max = 1_000;

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
//			long maxRecords = 1_000;
//			long maxRecords = 10_000;
			long maxRecords = 100_000;
//			long maxRecords = 1_000_000;
//			long maxRecords = 5_000_000;
			iteratorExample(maxRecords);

			concurrentMemoryStoreExample();
		};
	}

	public void concurrentMemoryStoreExample() {
		System.out.println("\nInit ConcurrentMemoryStore Example");
		ExecutorService service = Executors.newFixedThreadPool(2);
		service.execute(new ThreadOne());
		service.execute(new ThreadTwo());
		service.shutdownNow();
		System.out.println("\nEnd ConcurrentMemoryStore Example");
	}

	@SuppressWarnings("rawtypes")
	public void iteratorExample(long maxRecords) {
		System.out.println("\nInit Iterator Example");
//		Entrada: { [1,3,5,7,9,....] , [2,4,6,8,...], [0,10,20,30,...], ....}

		Collection<Iterator<Comparable>> l = new ArrayList<>();

		List<Long> secuence = new ArrayList<>();
		long sec = 1;
		for (long i = 0; i < maxRecords; i++) {
			secuence.add(sec);
			sec = sec + 2;
		}

		Iterator<Comparable> ds = new ComparableIterator(secuence);
		l.add(ds);
		secuence = new ArrayList<>();
		sec = 2;
		for (long i = 0; i < maxRecords; i++) {
			secuence.add(sec);
			sec = sec + 2;
		}
		ds = new ComparableIterator(secuence);
		l.add(ds);

		secuence = new ArrayList<>();
		sec = 0;
		for (long i = 0; i < maxRecords; i++) {
			secuence.add(sec);
			sec = sec + 10;
		}
		ds = new ComparableIterator(secuence);
		l.add(ds);

		SequenceIterator v2 = new SequenceIterator(l);

		int count = 0;
		while (v2.hasNext()) {
			Long f = (Long) v2.next();
			System.out.print(" " + f + " ");
			if (count == 20) {
				System.out.print("\n");
				count = 0;
			}
			count++;
		}
		System.out.println("\nEnd Iterator Example");
	}

	class ThreadOne implements Runnable {
		@Override
		public void run() {
			for (int i = max; i > 0; i--) {
				Item it = new Item();
				it.setValue1(i);
				it.setValue1(i + 1);
				try {
					store.store("Item-" + i, it);
				} catch (IllegalArgumentException e) {
					System.out.println("ThreadOne IllegalArgumentException " + e.getMessage());
				}
			}
			System.out.println("\nThreadOne iterate data");
			Iterator<Item> it = store.valueIterator();
			while (it.hasNext()) {
				Item item = it.next();
				System.out.println(item.toString());
			}
		}
	}

	class ThreadTwo implements Runnable {
		@Override
		public void run() {
			for (int i = 0; i < max; i++) {
				Item it = new Item();
				it.setValue1(i);
				it.setValue1(i + 1);
				try {
					store.store("Item-" + i, it);
				} catch (IllegalArgumentException e) {
					System.out.println("ThreadTwo IllegalArgumentException " + e.getMessage());
				}

			}
			System.out.println("\nThreadTwo iterate data");
			Iterator<Item> it = store.valueIterator();
			while (it.hasNext()) {
				Item item = it.next();
				System.out.println(item.toString());
			}
		}
	}

}
