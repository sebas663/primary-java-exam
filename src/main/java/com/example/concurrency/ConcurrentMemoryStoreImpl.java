package com.example.concurrency;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentMemoryStoreImpl implements ConcurrentMemoryStore {

	Map<String, Item> itemMap = new ConcurrentHashMap<String, Item>();

	@Override
	public void store(String key, Item item) throws IllegalArgumentException {
		if (item == null)
			throw new IllegalArgumentException("The entered value cannot be null");
		if (itemMap.containsKey(key))
			throw new IllegalArgumentException("The entered key already exists");
		itemMap.put(key, item);
	}

	@Override
	public void update(String key, int value1, int value2) {
		itemMap.computeIfPresent(key, (k, v) -> {
			v.setValue1(value1);
			v.setValue2(value2);
			return v;
		});
	}

	@Override
	public Iterator<Item> valueIterator() {
		return itemMap.values().iterator();
	}

	@Override
	public void remove(String key) {
		itemMap.remove(key);
	}

}
