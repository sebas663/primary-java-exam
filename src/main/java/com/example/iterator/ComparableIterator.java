package com.example.iterator;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@SuppressWarnings("rawtypes")
public class ComparableIterator implements Iterator<Comparable> {

	private List<? extends Comparable> secuence;
	private int pos = 0;

	public ComparableIterator(List<? extends Comparable> secuence) {
		super();
		Optional.ofNullable(secuence).orElseThrow(() -> new IllegalArgumentException("Unsupported value: " + secuence));
		this.secuence = secuence;
	}

	@Override
	public boolean hasNext() {
		if (pos >= secuence.size() || secuence.get(pos) == null)
			return false;
		else
			return true;
	}

	@Override
	public Comparable next() {
		Comparable next = secuence.get(pos);
		pos++;
		return next;
	}

}
