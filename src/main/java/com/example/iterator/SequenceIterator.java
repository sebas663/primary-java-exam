package com.example.iterator;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@SuppressWarnings("rawtypes")
public class SequenceIterator {

	private Collection<Iterator<Comparable>> inputs;
	private List<Comparable> output;
	private int pos = 0;

	public SequenceIterator(Collection<Iterator<Comparable>> inputs) {
		Optional.ofNullable(inputs).orElseThrow(() -> new IllegalArgumentException("Unsupported value: " + inputs));
		this.inputs = inputs;
		sort();
	}

	public boolean hasNext() {
		if (pos >= output.size() || output.get(pos) == null)
			return false;
		else
			return true;
	}

	public Comparable next() {
		Comparable next = output.get(pos);
		pos++;
		return next;
	}

	public void sort() {
		output = inputs.stream().flatMap(x -> getStreamFromIterator(x)).sorted().collect(Collectors.toList());
	}

	public Stream<Comparable> getStreamFromIterator(Iterator<Comparable> iterator) {
		Spliterator<Comparable> spliterator = Spliterators.spliteratorUnknownSize(iterator, 0);
		return StreamSupport.stream(spliterator, false);
	}

}
