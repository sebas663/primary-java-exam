# Run app con maven
```
mvn spring-boot:run
```

# Run app con java
```
mvn install
java -jar target/primary-java-exam-0.0.1-SNAPSHOT.jar
```